# Links Gatsby Starter Template

This Gatsby starter provides a linktree like site of pages for your own links from Instagram etc.

## How to use



### Link Page Structure

The link page is structured with a header, that comprises a header, with name / sub name, image, and a description.


#### Link Groups

This is followed by a collection of link groups, which can be used to organise links into meaningful groups, where a link group has the following attributes

* a unique ID
* a priority
* an image
* a title

The priority field is a number, and is used to sort the groups
Images are erxpected to be found in the `src/images` directory of the project

#### Links

Links are attached to a link group (so they display in the right place). Each link has the following attributes

* an unique id
* the unique id of the group it belongs to
* the link it should go to
* a priority
* an active flag
* an icon
* a title

The icon is one from the font-awesome library - see section below for details of using more complex items such as the duotone icons.

## Site Configuration

Setting up a links page is as simple as creating a JSON file in the `src/data/links` directory - an example file `starter.json` is provided.

When the gatsby develop / build scripts run, `gatsby-node.js` looks for any JSON files in `src/data/links` and will parse the file to create a page for it.

The JSON file is fairly self explanatory - it has the following key sections


### Header Data

Header data is in the root level fields,

* `id` - string - unique id, will be the name of the page created
* `active` - boolean - only files set as true will have pages created
* `name` - string - display name that will appear at the top of the file
* `isDefault` - boolean - if you have more than one page, this will be the one uses see if they access your site with no page requested (see below, "copy to index")
* `subname` - string - free text under the main name
* `image` - link to an image in the project, used for the hero image at the top of the page
* `imageStyle` - TBD
* `description` - string - the free text that appears below the hero image

### Groups

The `groups` property is an array of objects, which will be used to create the link groups. Groups are only displayed if they have links in them - therefore allowing you to create groups in advance if required.

Each group object has the folling attributes

* `id` - string - unique id, this is used by each link that will be in the group
* `priority` - integer - used to sort the groups - they will appear in numerical order - saves reorganising the array if you want to change the order things are displayed in
* `image` - link to an image in the project - this is shown above the title and is optional
* `title` - string - the title of the group

### Links

The `links` property is another array of objects at the root level





# Other things

## Font Awesome Icons

### Pro Support

    "@fortawesome/fontawesome-pro": "^5.15.1",
    "@fortawesome/pro-duotone-svg-icons": "^5.15.1",
    "@fortawesome/pro-light-svg-icons": "^5.15.1",
    "@fortawesome/pro-regular-svg-icons": "^5.15.1",
    "@fortawesome/pro-solid-svg-icons": "^5.15.1",
