import "@fortawesome/fontawesome-svg-core/styles.css"
import "@fontsource/ibm-plex-sans/400.css"
import "@fontsource/ibm-plex-sans/600.css"
import "@fontsource/ibm-plex-serif/200.css"         //Body
import "@fontsource/ibm-plex-serif/300.css"         //Titles (big)
import "@fontsource/ibm-plex-serif/400.css"         //Titles (little)
import "@fontsource/ibm-plex-serif/600.css"         //Highlighted text
