exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage, createRedirect } = actions





  const result = await graphql(`
    {
      allLinksJson(filter: {active: {eq: true}}) {
        edges {
          node {
            id
            isDefault
          }
        }
      }
    }
  `)

  // Handle errors
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const linkTemplate = require.resolve(`./src/templates/links.jsx`)

  //horrible bodge to clean up [Object: null prototype in the data]
  const trees = JSON.parse(JSON.stringify(result.data.allLinksJson.edges));

  //console.log(trees);

  //Create link pages


  trees.forEach(( {node} , i) => {
    //Create named page
    createPage({
      path: `/${node.id}`,
      component: linkTemplate,
      context: {
        // additional data can be passed via context
        id: node.id
      },
    })
  })

}


exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  const typeDefs = `
    type LinksJson implements Node {
      image: File @fileByRelativePath
      groups: [LinkGroup]
    },
    type LinkGroup {
      id: String!,
      priority: Int,
      title: String,
      image: File @fileByRelativePath,
      description: String
    }
  `
  createTypes(typeDefs)
}