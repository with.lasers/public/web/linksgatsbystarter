import * as React from "react"

import { GatsbyImage, getImage } from "gatsby-plugin-image"

const Header = ({ name, subname, image, description }) => {
  const Image = getImage(image)
  const RenderImage = Image ? <GatsbyImage image={Image} alt={name} className="rounded-3xl headerSafariRoundedImage"/> : ""

  return (
    <header>
      <div className="flex flex-col items-center my-8">
        <div className="headerName">{name}</div>
        <div className="mb-2 headerSubname">{subname}</div>
        <div className="m-2 p-2 ">{RenderImage}</div>
        <div className="px-6 text-justify headerDescription">{description}</div>
      </div>
    </header>
  )
}

export default Header
