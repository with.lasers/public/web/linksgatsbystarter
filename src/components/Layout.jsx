import * as React from "react"
import { Fragment } from "react"

import SEO from "./seo"
import "../style/global.css"

const Layout = ({ title, children }) => {
  return (
    <Fragment>
      <SEO title={title} />
      <div className="flex flex-row justify-center">
      <div className="">
      <main className="mx-4 flex flex-col content-center">{children}</main>
      <footer className="m-4">&copy; {new Date().getFullYear()} lxinspc / with.lasers</footer>
      </div>
      </div>
    </Fragment>
  )
}

export default Layout
