import * as React from "react"

import Layout from "../components/Layout"
import Header from "../components/Header"
import TreeGroup from "../components/TreeGroup"
import TreeLink from "../components/TreeLink"

export default function LinkPage({ linkData }) {


  const { name, subname, description, image, groups, links } = linkData;

  console.log(name, subname, description, image, groups, links);
  
  //Process groups and links, such that we can return components for LinkGroup and Link
  const GroupedLinks = groups.sort((a,b) => a.priority-b.priority).reduce((a,c) => {
    a.push({
      id: c.id,
      icon: c.icon,
      links: links.filter(f => f.group === c.id ).sort((a,b) => a.priority - b.priority),
      title: c.title,
      image: c.image,
      prefix: c.prefix
    });
    return a;
  },[]).filter(f => f.links.length > 0).map(group => {
    const links = group.links.map(link => {
      return (
        <TreeLink key={link.id} title={link.title} link={link.link} icon={link.icon} prefix={link.prefix} duo={link.duo}/>
      )
    })
    return (
      <TreeGroup key={group.id} title={group.title} image={group.image} icon={group.icon} prefix={group.prefix} duo={group.duo}> 
        {links}
      </TreeGroup>
    )
  });

  return (
    <Layout title={name}>
      <Header name={name} subname={subname} image={image} description={description}/>
      {GroupedLinks}
    </Layout>
  )
}