import * as React from "react"

import { GatsbyImage, getImage } from "gatsby-plugin-image"

const TreeGroup = ({ icon, title, image, children }) => {
  
  const Image = getImage(image);
  const RenderImage = Image ? (
    <GatsbyImage image={Image} alt={title}/>
  ) : '';

  return (
    <div className="my-4 mb-8 pb-4 border-b border-white border-opacity-20">
      {RenderImage}
      <div className="mb-2 groupTitle">
        {title}
      </div>
      <div>{children}</div>
    </div>
  )
}

export default TreeGroup
