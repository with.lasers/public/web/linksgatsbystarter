import * as React from "react"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import '../util/fontawesome.js';

const tailwind = require('../../tailwind.config');

const getTailWindColor = (sColor) => {
  const parts = sColor.split('-');
  const color = parts[0];
  const shade = (parts.length > 1 && parts[1]) || 'DEFAULT';
  return tailwind.theme.colors[color]?.[shade] || sColor;
};

const TreeLink = ({ title, link, icon, prefix, duo }) => {
  
  const Icon = () => {

    const duoStyle = (prefix === 'fad' && duo) ? {
      "--fa-primary-color": getTailWindColor(duo?.primary),
      "--fa-secondary-color": getTailWindColor(duo?.secondary),
      "--fa-secondary-opacity": 0.8
    } : {};  

    const swapOpacity = (prefix === 'fad' && duo?.swapOpacity) || false;

    return icon ? (
      <FontAwesomeIcon icon={[prefix || 'fas', icon]} className="iconColor" fixedWidth style={duoStyle} swapOpacity={swapOpacity}/>
    ) : ''
  }
  
  
  return (
    <div>
      <a href={link} target="_blank" rel="noreferrer" className="flex flex-row items-center my-1 linkTitle">
        {Icon()}
        <div className="pl-2 linkTitleText">
          {title}
        </div>
      </a>
    </div>
  )
}

export default TreeLink
