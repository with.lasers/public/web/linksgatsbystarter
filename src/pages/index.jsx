import * as React from "react"
import { graphql } from "gatsby"

import LinkPage from "../components/LinkPage"

export default function Index({ data, pageContext, location }) {

  //Find a default page
  console.log(data);

  const defaultLinkData = data.allLinksJson.nodes.find(f=>f.isDefault) || data.allLinksJson.nodes[0];

  console.log(defaultLinkData);

  return (
   <LinkPage linkData={defaultLinkData}/>
  )



}



export const pageQuery = graphql`
query {
  allLinksJson {
    nodes {
      id
      active
      description
      groups {
        id
        priority
        title
        image {
          childImageSharp {
            gatsbyImageData(layout: CONSTRAINED, height: 100, placeholder: TRACED_SVG)
          }
        }
      }
      image {
        childImageSharp {
          gatsbyImageData(layout: CONSTRAINED, height: 150, placeholder: TRACED_SVG)
        }
      }
      imageStyle
      isDefault
      name
      subname
      links {
        active
        group
        icon
        id
        link
        prefix
        priority
        title
      }
    }
  }
}
`
