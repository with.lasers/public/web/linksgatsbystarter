import * as React from "react"
import { graphql } from "gatsby"

import LinkPage from "../components/LinkPage"

export default function Template({ data, pageContext, location }) {

  return (
    <LinkPage linkData={data.linksJson}/>
  )

}

export const pageQuery = graphql`
query($id: String!) {
  linksJson(id: {eq: $id}) {
    groups {
      id
      priority
      title
      image {
        childImageSharp {
          gatsbyImageData(layout: CONSTRAINED, height: 100, placeholder: TRACED_SVG)
        }
      }
    }
    description
    id
    image {
      childImageSharp {
        gatsbyImageData(layout: CONSTRAINED, height: 150, placeholder: TRACED_SVG)
      }
    }
    links {
      active
      group
      id
      link
      priority
      title
      icon
      prefix
    }
    name
    subname
  }
}
`
