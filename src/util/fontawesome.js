import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'

//Free only regular support
import { far } from '@fortawesome/free-regular-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'

// Additional font awesome pro libraries

// import { fas } from '@fortawesome/pro-solid-svg-icons'
// import { far } from '@fortawesome/pro-regular-svg-icons'
// import { fal } from '@fortawesome/pro-light-svg-icons'
// import { fad } from '@fortawesome/pro-duotone-svg-icons'

// use if using free fonts only
library.add(fab, fas, far);

// use if making use of pro fonts
// library.add(fab, fas, far, fal, fad)