module.exports = {
  purge: {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    options: {
      whitelist: [],
    },
  },
  darkMode: false, // or 'media' or 'class'
}